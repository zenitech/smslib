﻿using System;
namespace AirboxShortMessageService
{
    public interface ShortMessageService
    {
        void Send(String from, String to, String body);
        void AddResponseHandler(MessageServiceBase.ResponseHandler handler);
    }
}
