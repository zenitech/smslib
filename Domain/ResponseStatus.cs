﻿namespace AirboxShortMessageService.Domain
{
    public enum ResponseStatus
    {
        SENT,
        FAILED,
        UNKNOWN
    }
}

