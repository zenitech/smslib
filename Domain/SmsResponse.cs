﻿using System;

namespace AirboxShortMessageService.Domain
{
    public class SmsResponse
    {
        public Object ActualResponse { get; set; }
        public Type ResponseType { get; set; }
        public ResponseStatus Status { get; set; }
    }
}
