﻿using AirboxShortMessageService.Domain;
using AirboxShortMessageService.Internal.Twilio;
using System;

namespace AirboxShortMessageService
{
    public class ShortMessageServiceFactory
    {
       
        public static ShortMessageService Create()
        {
            var credentials = CreateCredentials();
            return new TwilioShortMessageService(credentials);
        }

        private static Credentials CreateCredentials()
        {   
            String accountSID = Properties.Resources.AccountSID;
            String authToken = Properties.Resources.AuthToken;
            return new Credentials(accountSID, authToken);
        }      
    }
}
