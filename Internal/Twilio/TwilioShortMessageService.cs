﻿
using AirboxShortMessageService.Domain;
using RestSharp.Validation;
using System;
using Twilio;

namespace AirboxShortMessageService.Internal.Twilio
{
    public class TwilioShortMessageService : MessageServiceBase
    {
        private Credentials credentials { get; }
        private TwilioRestClient twilio;
        
        internal TwilioShortMessageService(Credentials credentials)
        {
            Require.Argument("credentials", credentials);
            twilio = new TwilioRestClient(credentials.accountSID, credentials.authToken);
        }

        public override void Send(String from, String to, String body) {
            Require.Argument("from", from);
            Require.Argument("to", to);
            Require.Argument("body", body);

            twilio.SendMessage(from, to, body, HandleResponse);
        }
        
        public void HandleResponse(Message message)
        {
            ResponseStatus responseStatus = GetResponseStatus(message.Status);
            SmsResponse response = new SmsResponse{
                ResponseType = typeof(Message),
                ActualResponse = message,
                Status = responseStatus
            };

            PropagateResponse(response);
        }

        private ResponseStatus GetResponseStatus(String twilioStatus)
        {
            switch (twilioStatus)
            {
                case "Sent":
                    return ResponseStatus.SENT;
                case "Failed":
                    return ResponseStatus.FAILED;
                default:
                    return ResponseStatus.UNKNOWN;
            }
        }
    }
}
