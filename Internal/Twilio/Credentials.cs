﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirboxShortMessageService.Internal.Twilio
{
    internal class Credentials
    {
        public String accountSID { get; }
        public String authToken { get; }
        public Credentials(String accountSID, String authToken)
        {
            this.accountSID = accountSID;
            this.authToken = authToken;
        }
    }
}
