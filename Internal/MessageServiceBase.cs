﻿using AirboxShortMessageService.Domain;

namespace AirboxShortMessageService
{
    public abstract class MessageServiceBase : ShortMessageService
    {
        public delegate void ResponseHandler(SmsResponse response);
        private ResponseHandler handlers;
        public abstract void Send(string from, string to, string body);

        protected void PropagateResponse(SmsResponse response) {
            handlers(response);
        }

        public void AddResponseHandler(ResponseHandler handler)
        {
            handlers += handler;
        }
    }
}
